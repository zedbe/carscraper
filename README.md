# CarScraper
This project is an useful tool for web scraping car informations from car sale webpages. Written in Python v3.6+.
Output can be used in e.g. some AI computations.

Here is list of scraped targets: [sauto.cz](https://www.sauto.cz/osobni/hledani#!category=1&condition=1&condition=2&condition=4)

What data scraper collects:

ID  | brand | model | year of manufacture   | mileage (km)  | actual price  | fuel  | URL
--- | ---   | ---   |---                    | ---           | ---           | ---   | ---
0   | Volvo | V60   | 2019                  | 18000         | 849000        | ---   | https://www.sauto.cz/osobni/detail/volvo/v60/17902588
1   | ...   | ...   | ...                   | ...           | ...           | ---   | ...