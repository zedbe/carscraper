# %%
import requests
import pandas as pd
from datetime import datetime, timedelta
import os
from carscraper.create_logger import create_logger


def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def extract_brands():
    url_brands = "https://www.sauto.cz/?ajax=1&condition=1&condition=2&condition=4&category=1"
    res = requests.get(url_brands)
    json = res.json()
    brands = []

    # A List of manufacturers
    manufacturers = json['topManufacturers'] + json['otherManufacturers']
    man_len = len(manufacturers)

    # Initial call to print 0% progress
    print_progress_bar(0, man_len, prefix='Progress:', suffix='Complete', length=50)

    for i, brand in enumerate(manufacturers):
        if int(brand['size']) > 0:
            brands.append(CarBrand(brand['name'], brand['url'], brand['size'], brand['val']))

        # Update Progress Bar
        print_progress_bar(i + 1, man_len, prefix='Progress:', suffix='Completed {0}/{1}'.format(i + 1, man_len),
                           length=50)

    return brands


def create_url(model_path, id):
    return "http://sauto.cz/inzerce/{0}{1}".format(model_path, id)


# modeling classes to transform from website
class CarBrand:
    def __init__(self, name, url, count, val):
        self.name = name
        self.id_brand = val
        self.url = url
        self.count = count
        self.models = []
        self.extract_models()

    def extract_models(self):
        url = "https://www.sauto.cz/hledani?ajax=2&page=1&condition=4&condition=2&condition=1&category=1" \
              "&manufacturer={0}".format(self.id_brand)
        response = requests.get(url)

        if response.status_code == 200:
            code_book = response.json().get('filter').get('manufacturer')[0].get('codebookModel')

            for model in code_book:
                self.models.append(CarModel(model['name'], model['val'], model['size'], self.id_brand))


class CarModel:
    def __init__(self, name, id_model, count, id_brand):  # , year, mileage, price):
        self.name = name  # 2
        self.id_model = id_model
        self.count = count
        self.cars = []
        if int(count) > 15 * 667:
            print("Be aware! response returns only 15 items per page and maximum page number is 667. If user wants to "
                  "list more cars, filter must be changed (price typically).\n")
        self.extract_cars(id_brand, self.id_model)

    def extract_cars(self, manufacturer_id, model_id):
        actual_page = 1
        while True:
            url = "https://www.sauto.cz/hledani?ajax=2&page={page}&condition=4&condition=2&condition=1&category=1&" \
                  "manufacturer={man_id}&model={mod_id}".format(page=actual_page, man_id=manufacturer_id,
                                                                mod_id=model_id)
            response = requests.get(url)
            if response.status_code == 200:
                json = response.json()
                total_pages = json.get('paging')['totalPages']

                for advert in json.get('advert'):
                    # one of json properties can be 0, this will avoid the zero value
                    if int(advert['advert_run_date']) >= int(advert['advert_made_date']):
                        init_year = advert['advert_run_date']
                    else:
                        init_year = advert['advert_made_date']
                    self.cars.append(
                        Car(init_year, advert['advert_tachometr'], advert['advert_price_total'],
                            advert['advert_fuel_cb'], advert['advert_gearbox_cb'],
                            create_url(advert['advert_url'], advert['advert_id'])))

                if actual_page < total_pages:
                    actual_page += 1
                else:
                    break


class Car:
    def __init__(self, year, mileage, price, fuel, gearbox, url):
        self.year = year
        self.mileage = mileage
        self.price = price
        self.fuel = fuel
        self.gearbox = gearbox
        self.complete_url = url


def list_dict_2_list_list(brands):
    records = []

    for brand in brands:
        for model in brand.models:
            for car in model.cars:
                records.append([brand.name, model.name, car.year, car.mileage, car.price, car.fuel, car.gearbox,
                                car.complete_url])
    return records


def make_info_log(start, logger):
    end = datetime.now()
    diff_in_minutes = (end - start) / timedelta(minutes=1)

    logger.info("Scraping started:\t{0}".format(start))
    logger.info("Scraping end:\t\t{0}".format(end))
    logger.info("Total estimation:\t{0} min".format(diff_in_minutes))


#%%
if __name__ == "__main__":
    start_timestamp = datetime.now()

    output_root = "outputs"
    output_dir = "sauto_output_{0}-{1}-{2}".format(start_timestamp.year, start_timestamp.month, start_timestamp.day)
    output_file = output_dir + "_{0}-{1}-{2}".format(start_timestamp.hour, start_timestamp.minute,
                                                     start_timestamp.second)

    if not os.path.isdir('./{0}'.format(output_root)):
        os.mkdir('./{0}'.format(output_root))

    if not os.path.isdir("./{0}/{1}".format(output_root, output_dir)):
        os.mkdir("./{0}/{1}".format(output_root, output_dir))

    logger = create_logger("./{0}/{1}/{2}.log".format(output_root, output_dir, output_file))

    brands = list_dict_2_list_list(extract_brands())
    columns = ["brand", "model", "year", "mileage", "price", "fuel", "gearbox", "url"]
    df = pd.DataFrame(brands, columns=columns)

    df.to_csv('./{0}/{1}/{2}.csv'.format(output_root, output_dir, output_file))
    make_info_log(start_timestamp, logger)
